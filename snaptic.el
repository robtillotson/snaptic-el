
;;
;;  Emacs interface to snaptic.com notes.
;;
;;  Snaptic provides a no-cost notetaking application for
;;  mobile phones, with web synchronization and a REST/JSON
;;  API.
;;
(require 'json)

(defun snaptic-get-json (url)
  (let (status headers data ret)
    (with-current-buffer (url-retrieve-synchronously url)
      (setq status url-http-response-status)
      (when (eql status 200)
        (goto-char (point-min))
        (search-forward-regexp "^$" nil t)
        (setq headers (buffer-substring (point-min) (point)))
        (setq data (buffer-substring (1+ (point)) (point-max)))
        (setq ret (json-read-from-string data))))
    ret))

(defun snaptic-get-all-notes ()
  (snaptic-get-json "https://api.snaptic.com/v1/notes"))

(defun snaptic-get-note (id)
  (let ((ret (snaptic-get-json (format "https://api.snaptic.com/v1/notes/%s" id))))
    (when ret
      (elt (cdr (assq 'notes ret)) 0))))


